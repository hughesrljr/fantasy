<?php

namespace Tests\Unit;

use App\PlayersModel;
use Tests\TestCase;

class PlayerTest extends TestCase
{
    public function test_get_all_players() {
        // Create 100 Users then call the API to list all 100 players
        // 0 player in the list will result a 404 status code since no record in the data
        $playersFactory = factory(PlayersModel::class, 100)->create();

        $this->get(route('players.index'))
            ->assertStatus(200);
    }

    public function test_get_player_by_id() {

        $playerModel = factory(PlayersModel::class)->create();

        $this->get(route('players.info.byid', $playerModel->fpl_id))
            ->assertStatus(200);
    }

    public function test_get_player_by_name() {

        $playerModel = factory(PlayersModel::class)->create();

        $this->get(route('players.info.byname', $playerModel->fpl_first_name))
            ->assertStatus(200);
    }

    public function test_get_player_by_id_failed() {

        $this->get(route('players.info.byid', 0))
            ->assertStatus(404);
    }

    public function test_get_player_by_name_failed() {
        $this->get(route('players.info.byid', 'Rob'))
            ->assertStatus(404);
    }

}
