<?php

namespace App\Http\Controllers;

use App\PlayersModel;
use Illuminate\Http\Request;

class PlayerController extends Controller
{
    public function index() {
    	$getAll = PlayersModel::getAll();
    	if($getAll == false) {
    		return response()->json(['error'=>'No Players found'], 404); 
    	}

	    return response()->json($getAll, 200);
    }

    public function getPlayerById($playerId) {
    	$getById = PlayersModel::getById($playerId);
    	if($getById == false) {
    		return response()->json(['error'=>'Record does not exists'], 404); 
    	}

	    return response()->json($getById, 200);
    }

    public function getPlayerByName($playerName) {
    	$getByName = PlayersModel::getByName($playerName);
    	if($getByName == false) {
    		return response()->json(['error'=>$playerName.' Player does not exist'], 404); 
    	}

	    return response()->json($getByName, 200);

    }
}
