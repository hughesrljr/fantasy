<?php

namespace App;

use Illuminate\Support\Facades\Log;

use DB;
use Storage;
use File;
use DateTime;
use DateTimeZone;

use App\PlayersModel;

use App\TelegramApi;

class FantasyModelApi {
    const PLAYER_LIMIT = 100;

    protected $domainUrl;

	public function __construct() {
		$this->domainUrl = env("FANTACY_URL", "https://fantasy.premierleague.com/");
	}

	public function syncFantasyPlayers() {
        $curlFANTACY = $this->curlRequest($this->domainUrl."api/bootstrap-static/");
        
        if($curlFANTACY['status_code'] == 200) {
            $aFantacyPlayers = array_slice($curlFANTACY['elements'], 0, static::PLAYER_LIMIT);
            foreach ($aFantacyPlayers as $pKey => $aFantacyPlayersInfo) {
                
                $fullnameParsed = $aFantacyPlayersInfo["first_name"]." ".$aFantacyPlayersInfo["second_name"];
                $aFantacyPlayersInfo['full_name'] = $fullnameParsed;
                
                $this->parsePlayerInfo($aFantacyPlayersInfo);
            }
        }        
	}

    private function parsePlayerInfo($playerInfo) {
        foreach ($playerInfo as $key => $playerInfoValue) {
            $newKey = preg_replace("/(_?\d+)+$/","",$key);
            $arrPlayerInfoKeys['fpl_'.$newKey] = $playerInfoValue;

            if('fpl_'.$newKey == "fpl_news_added" && !empty($playerInfoValue)) {
                $dateConverted = date("Y-m-d H:i:s", strtotime($playerInfoValue));
                echo $playerInfoValue." = ".$dateConverted."\n";
                $arrPlayerInfoKeys['fpl_'.$newKey] = $dateConverted;
            }
        }

        PlayersModel::updateOrCreate(
            ['fpl_id' => $arrPlayerInfoKeys['fpl_id']],
            $arrPlayerInfoKeys
        );

        return $arrPlayerInfoKeys;
    }
    public function curlRequest($url) {
        $result = ['error' => false, 'mesasge' => 'success', 'status_code' => 200, 'headers' => false];

        $handle = curl_init();

        curl_setopt($handle, CURLOPT_URL, $url);
        // curl_setopt($handle, CURLOPT_URL, "http://api.plos.org/search?q=title:%22Drosophila%22%20and%20body:%22RNA%22&fl=id,abstract&wt=xml&indent=on");
        curl_setopt($handle, CURLOPT_HEADER, true);
        curl_setopt($handle, CURLOPT_RETURNTRANSFER, true);

        $responseCurlExec = curl_exec($handle);

        $result['status_code'] = curl_getinfo($handle, CURLINFO_HTTP_CODE);

        $curl_error = curl_error($handle);
        $header_size = curl_getinfo($handle, CURLINFO_HEADER_SIZE);

        curl_close($handle);

        $result['headers'] = $this->get_headers_from_curl_response(substr($responseCurlExec, 0, $header_size));
        $result['contentType'] = $result['headers']['Content-Type'];
        $body = substr($responseCurlExec, $header_size);
        $curlResult = $this->parseResponse($result['headers']['Content-Type'], $body);
        
        $curlResult = json_decode($body, true);

        if($result['status_code'] != 200) {
            Log::info("FANTACY : CURL \n\n Domain: ".$url."\nStatus Code: ".$result['status_code']." \n Error Message: ". $curl_error . "\n Response Message: ".$curlResult);
        }
        if($curlResult == false) {
            Log::info("FANTACY : CURL \n\n Domain: ".$url."\nStatus Code: ".$result['status_code']." \n Error Message: No response received from the Domain");
        }

        $result = array_merge_recursive($result, $curlResult);

        return $result;
    }

    function get_headers_from_curl_response($response) {
        $headers = array();

        $header_text = substr($response, 0, strpos($response, "\r\n\r\n"));

        foreach (explode("\r\n", $header_text) as $i => $line)
            if ($i === 0) {
                $headers['http_code'] = $line;
            } else {
                list ($key, $value) = explode(': ', $line);
                $headers[$key] = $value;
            }

        return $headers;
    }

    private function parseResponse($mimeType="application/json", $contentData) {
        if(strpos(strtolower($mimeType), "xml") !== false) {
            $contentData = simplexml_load_string($contentData);
            return json_encode($contentData, true);
        }
        return json_decode($contentData);
    }
}
