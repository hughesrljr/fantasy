<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PlayersModel extends Model
{
    protected $table = 'players';

    protected $primaryKey = 'id';

    public $timestamps = false;

    protected $fillable = [
        'fpl_chance_of_playing_next_round',
        'fpl_chance_of_playing_this_round',
        'fpl_code',
        'fpl_cost_change_event',
        'fpl_cost_change_event_fall',
        'fpl_cost_change_start', 
        'fpl_cost_change_start_fall',
        'fpl_dreamteam_count',
        'fpl_element_type',
        'fpl_ep_next',
        'fpl_ep_this',
        'fpl_event_points',
        'fpl_full_name',
        'fpl_first_name',
        'fpl_form',
        'fpl_id',
        'fpl_in_dreamteam',
        'fpl_news',
        'fpl_news_added',
        'fpl_now_cost',
        'fpl_photo',
        'fpl_points_per_game', 
        'fpl_second_name',
        'fpl_selected_by_percent',
        'fpl_special',
        'fpl_squad_number',
        'fpl_status', 
        'fpl_team', 
        'fpl_team_code', 
        'fpl_total_points', 
        'fpl_transfers_in',
        'fpl_transfers_in_event',
        'fpl_transfers_out',
        'fpl_transfers_out_event',
        'fpl_value_form',
        'fpl_value_season',
        'fpl_web_name',
        'fpl_minutes',
        'fpl_goals_scored',
        'fpl_assists',
        'fpl_clean_sheets',
        'fpl_goals_conceded',
        'fpl_own_goals',
        'fpl_penalties_saved',
        'fpl_penalties_missed',
        'fpl_yellow_cards',
        'fpl_red_cards',
        'fpl_saves',
        'fpl_bonus',
        'fpl_bps',
        'fpl_influence',
        'fpl_creativity',
        'fpl_threat',
        'fpl_ict_index'
    ];

    static function getAll() {
        $searchResult = self::select('fpl_id as id','fpl_full_name as name')->get();

        if($searchResult->isEmpty()) {
            return false; 
        }

        return $searchResult->toArray();
    }

    static function getById($playerId) {
        $searchResult = PlayersModel::select('fpl_id as id','fpl_first_name as firstname','fpl_second_name as secondname',
            'fpl_value_form as form','fpl_total_points as total_points','fpl_influence as influence','fpl_creativity as creativity',
            'fpl_threat as threat','fpl_ict_index as ict_index')
            ->whereFplId($playerId)
            ->get();

        if($searchResult->isEmpty()) {
            return false; 
        }

        
        return $searchResult->toArray();
    }

    static function getByName($playerName) {
        $searchByName = PlayersModel::select('fpl_id as id','fpl_first_name as firstname','fpl_second_name as secondname',
            'fpl_value_form as form','fpl_total_points as total_points','fpl_influence as influence','fpl_creativity as creativity',
            'fpl_threat as threat','fpl_ict_index as ict_index')
            ->orderBy('fpl_first_name');

        $extractSpaces = explode(" ",strtolower($playerName));
        $nNumOfCondition = count($extractSpaces);

        $full_name_condition = $extractSpaces[0]."%";
        if($nNumOfCondition > 1) {
            $full_name_condition .= ' '.strtolower(end($extractSpaces))."%";
        }

        $searchByName->orWhere("fpl_full_name", 'like', $full_name_condition);

        $searchResult = $searchByName->get();
        if($searchResult->isEmpty()) {
            return false; 
        }

        return response()->json($searchResult->toArray(), 200);
    }


}
