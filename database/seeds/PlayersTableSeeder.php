<?php

use Illuminate\Database\Seeder;
use App\FantasyModelApi;

class PlayersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // FantasyModelApi::truncate();

        // $faker = \Faker\Factory::create();

        // // And now, let's create a few articles in our database:
        // for ($i = 0; $i < 50; $i++) {
        //     FantasyModelApi::create([
        //     	'fpl_chance_of_playing_next_round' => $faker->numberBetween(1,100),
		      //   'fpl_chance_of_playing_this_round' => $faker->numberBetween(1,100),
		      //   'fpl_code' => $faker->numberBetween(1,100),
		      //   'fpl_cost_change_event',
		      //   'fpl_cost_change_event_fall',
		      //   'fpl_cost_change_start', 
		      //   'fpl_cost_change_start_fall',
		      //   'fpl_dreamteam_count',
		      //   'fpl_element_type',
		      //   'fpl_ep_next',
		      //   'fpl_ep_this',
		      //   'fpl_event_points',
		      //   'fpl_first_name',
		      //   'fpl_form',
		      //   'fpl_id',
		      //   'fpl_in_dreamteam',
		      //   'fpl_news',
		      //   'fpl_news_added',
		      //   'fpl_now_cost',
		      //   'fpl_photo',
		      //   'fpl_points_per_game', 
		      //   'fpl_second_name',
		      //   'fpl_selected_by_percent',
		      //   'fpl_special',
		      //   'fpl_squad_number',
		      //   'fpl_status', 
		      //   'fpl_team', 
		      //   'fpl_team_code', 
		      //   'fpl_total_points', 
		      //   'fpl_transfers_in',
		      //   'fpl_transfers_in_event',
		      //   'fpl_transfers_out',
		      //   'fpl_transfers_out_event',
		      //   'fpl_value_form',
		      //   'fpl_value_season',
		      //   'fpl_web_name',
		      //   'fpl_minutes',
		      //   'fpl_goals_scored',
		      //   'fpl_assists',
		      //   'fpl_clean_sheets',
		      //   'fpl_goals_conceded',
		      //   'fpl_own_goals',
		      //   'fpl_penalties_saved',
		      //   'fpl_penalties_missed',
		      //   'fpl_yellow_cards',
		      //   'fpl_red_cards',
		      //   'fpl_saves',
		      //   'fpl_bonus',
		      //   'fpl_bps',
		      //   'fpl_influence',
		      //   'fpl_creativity',
		      //   'fpl_threat',
		      //   'fpl_ict_index'
        //         'title' => $faker->sentence,
        //         'body' => $faker->paragraph,
        //     ]);
        // }
    }
}
