<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePlayersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
    //     Schema::create('players', function (Blueprint $table) {
    //         $table->bigIncrements('id');
    //         $table->string('fpl_full_name', 150)->index('fpl_full_name');
    //         $table->string('fpl_first_name', 150)->index('fpl_first_name');
    //         $table->decimal('fpl_form',8,2)->default(0.0);
    //         $table->integer('fpl_id')->default(0)->index('fpl_id');
    //         $table->string('fpl_second_name', 150)->index('fpl_second_name');
    //         $table->string('fpl_status', 150)->nullable(); 
    //         $table->integer('fpl_team')->default(0); 
    //         $table->integer('fpl_team_code')->default(0); 
    //         $table->integer('fpl_total_points')->default(0); 
    //         $table->decimal('fpl_influence',8,2)->default(0.0);
    //         $table->decimal('fpl_creativity',8,2)->default(0.0);
    //         $table->decimal('fpl_threat',8,2)->default(0.0);
    //         $table->decimal('fpl_ict_index',8,2)->default(0.0);
    //         $table->timestamp('created_at')->default(DB::raw('CURRENT_TIMESTAMP'));
    //         $table->timestamp('updated_at')->default(DB::raw('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'));
    //     });
    // }
        Schema::create('players', function (Blueprint $table) {
            $table->bigIncrements('fpl_prime_id');
            $table->integer('fpl_chance_of_playing_next_round')->nullable()->default(0);
            $table->integer('fpl_chance_of_playing_this_round')->nullable()->default(0);
            $table->integer('fpl_code')->default(0);
            $table->integer('fpl_cost_change_event')->default(0);
            $table->integer('fpl_cost_change_event_fall')->default(0);
            $table->integer('fpl_cost_change_start')->default(0); 
            $table->integer('fpl_cost_change_start_fall')->default(0);
            $table->integer('fpl_dreamteam_count')->default(0);
            $table->integer('fpl_element_type')->default(0);
            $table->decimal('fpl_ep_next',8,2)->default(0.0);
            $table->decimal('fpl_ep_this',8,2)->default(0.0);
            $table->integer('fpl_event_points')->default(0);
            $table->string('fpl_full_name')->index('fpl_full_name');
            $table->string('fpl_first_name', 150)->index('fpl_first_name');
            $table->decimal('fpl_form',8,2)->default(0.0);
            $table->integer('fpl_id')->default(0)->index('fpl_id');
            $table->boolean('fpl_in_dreamteam')->default(0);
            $table->text('fpl_news')->nullable();
            $table->dateTimeTz('fpl_news_added')->nullable();
            $table->integer('fpl_now_cost')->default(0);
            $table->string('fpl_photo', 150)->nullable();
            $table->decimal('fpl_points_per_game',8,2)->default(0.0); 
            $table->string('fpl_second_name', 150)->index('fpl_second_name');
            $table->decimal('fpl_selected_by_percent',8,2)->default(0.0);
            $table->boolean('fpl_special')->default(0);
            $table->integer('fpl_squad_number')->nullable()->default(0);
            $table->string('fpl_status', 150)->nullable(); 
            $table->integer('fpl_team')->default(0); 
            $table->integer('fpl_team_code')->default(0); 
            $table->integer('fpl_total_points')->default(0); 
            $table->integer('fpl_transfers_in')->default(0);
            $table->integer('fpl_transfers_in_event')->default(0);
            $table->integer('fpl_transfers_out')->default(0);
            $table->integer('fpl_transfers_out_event')->default(0);
            $table->decimal('fpl_value_form', 8,2)->default(0.0);
            $table->decimal('fpl_value_season', 8,2)->default(0.0);
            $table->string('fpl_web_name', 150)->nullable()->index('fpl_web_name');
            $table->integer('fpl_minutes')->default(0);
            $table->integer('fpl_goals_scored')->default(0);
            $table->integer('fpl_assists')->default(0);
            $table->integer('fpl_clean_sheets')->default(0);
            $table->integer('fpl_goals_conceded')->default(0);
            $table->integer('fpl_own_goals')->default(0);
            $table->integer('fpl_penalties_saved')->default(0);
            $table->integer('fpl_penalties_missed')->default(0);
            $table->integer('fpl_yellow_cards')->default(0);
            $table->integer('fpl_red_cards')->default(0);
            $table->integer('fpl_saves')->default(0);
            $table->integer('fpl_bonus')->default(0);
            $table->integer('fpl_bps')->default(0);
            $table->decimal('fpl_influence',8,2)->default(0.0);
            $table->decimal('fpl_creativity',8,2)->default(0.0);
            $table->decimal('fpl_threat',8,2)->default(0.0);
            $table->decimal('fpl_ict_index',8,2)->default(0.0);
            $table->timestamp('created_at')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updated_at')->default(DB::raw('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'));
            // $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('players');
    }
}