<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\PlayersModel;
use Faker\Generator as Faker;

$factory->define(PlayersModel::class, function (Faker $faker) {
	$firstName = $faker->firstName;
    $lastName = $faker->lastName;
	return [
        'fpl_full_name' => $firstName.' '.$lastName,
        'fpl_first_name' => $firstName,
        'fpl_form' => $faker->randomFloat(3, 0, 1000),
        'fpl_id' => $faker->randomDigit,
        'fpl_second_name' => $lastName,
        'fpl_influence' => $faker->randomFloat(3, 0, 1000),
        'fpl_creativity' => $faker->randomFloat(3, 0, 1000),
        'fpl_threat' => $faker->randomFloat(3, 0, 1000),
        'fpl_ict_index' => $faker->randomFloat(3, 0, 1000)
	];
});
