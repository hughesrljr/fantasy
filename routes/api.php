<?php

use Illuminate\Http\Request;

Use App\FantasyPlayersModel;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::get('players', function () {
//     $searchResult = FantasyPlayersModel::select('fpl_id as id','fpl_full_name as name')->get();

//     if($searchResult->isEmpty()) {
//         return response()->json(['error'=>'No Players found'], 404); 
//     }

//     return response()->json($searchResult->toArray(), 200);
// })->name('fantasy.players');

// Route::get('players/{id}', function($id) {
//     $searchResult = FantasyPlayersModel::select('fpl_id as id','fpl_first_name as firstname','fpl_second_name as secondname',
//         'fpl_value_form as form','fpl_total_points as total_points','fpl_influence as influence','fpl_creativity as creativity',
//         'fpl_threat as threat','fpl_ict_index as ict_index')
//         ->whereFplId($id)
//         ->get();

//     if($searchResult->isEmpty()) {
//         return response()->json(['error'=>'Record does not exists'], 404); 
//     }

//     return response()->json($searchResult->toArray(), 200);
// })->where('id', '[0-9]+')->name('fantasy.playersbyid');

// Route::get('players/{name?}', function($name) {
// 	$searchByName = FantasyPlayersModel::select('fpl_id as id','fpl_first_name as firstname','fpl_second_name as secondname',
//         'fpl_value_form as form','fpl_total_points as total_points','fpl_influence as influence','fpl_creativity as creativity',
//         'fpl_threat as threat','fpl_ict_index as ict_index')
//         ->orderBy('fpl_first_name');
// 	$extractSpaces = explode(" ",$name);
//     $nNumOfCondition = count($extractSpaces);

//     $full_name_condition = strtolower($extractSpaces[0])."%";
//     if($nNumOfCondition > 1) {
//     	$full_name_condition .= ' '.strtolower(end($extractSpaces))."%";
//     }
//     $searchByName->orWhere("fpl_full_name", 'like', $full_name_condition);

//     $searchResult = $searchByName->get();
//     if($searchResult->isEmpty()) {
//     	return response()->json(['error'=>$name.' Player does not exist'], 404); 
//     }

//     return response()->json($searchResult->toArray(), 200);
// })->name('fantasy.playersbyname');


Route::get('/players', 'PlayerController@index')->name('players.index');
Route::get('players/{playerId}', 'PlayerController@getPlayerById')->where('playerId', '[0-9]+')->name('players.info.byid');
Route::get('players/{playerName?}', 'PlayerController@getPlayerByName')->name('players.info.byname');