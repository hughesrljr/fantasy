1. Set the env content
	1.1 Database connection
	1.2 Add this to to .env 
		FANTACY_URL=https://fantasy.premierleague.com/
		
2. create the database
3. composer update --no-scripts
4. php artisan key:generate
5. php artisan config:cache
6. Unit Testing
	6.1. Please run this command. 
		./vendor/bin/phpunit
		
7. php artisan migrate
8. php artisan fantasy:players
9. php -S  127.0.0.1:9000 -t public/
10. Open in browser http://127.0.0.1:9000/api/players - to show all the 100 players
11. http://127.0.0.1:9000/api/players/99 - Get Player Information by ID (fpl_id)
12. http://127.0.0.1:9000/api/players/Nacho Monreal - Get Player Information by Name [Firstname and/or Lastname] or http://127.0.0.1:9000/api/players/And
13. To make the fantasy:players run in cron you need to setup the ff. (The schedule for the command to run is every 1 minute without overlapping with other scheduled commands)
	12.1. crontab -e
	12.2. * * * * * cd /path-to-your-project && php artisan schedule:run >> /dev/null 2>&1